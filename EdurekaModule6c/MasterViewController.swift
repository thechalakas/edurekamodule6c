//
//  MasterViewController.swift
//  EdurekaModule6c
//
//  Created by Jay on 18/02/18.
//  Copyright © 2018 the chalakas. All rights reserved.
//

import UIKit

class MasterViewController : UITableViewController
{
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        print("MasterViewController has loaded")
    }
    
    //set the number of rows.
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
        print("tableView numberOfRowsInSection begins")
        
        print("tableView numberOfRowsInSection ends")
        //we just need to inform the table how many rows there will be.
        return 10
    }
    
    //set the data for each row.
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        print("tableView cellForRowAt begins")
        //we need a UITableViewCell
        //this is the way to create new cells each time you want it.
        let cell = UITableViewCell(style: .value1, reuseIdentifier: "UITableViewCell")
        

        
        
        //attach things from item to the cell
        cell.textLabel?.text = "simple name"
        cell.detailTextLabel?.text = "$\(50)"
        
        print("tableView cellForRowAt ends")
        
        //return the cell to be displayed in the row
        return cell
    }
}
